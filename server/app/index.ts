import * as express from "express";
import { logit } from "./utils";
import { ServerStateUser } from "library/interfaces";
const socketIO = require("socket.io");
const generate = require("nanoid/generate");

const app = express();
const server = express()
  .use(app)
  .listen(8070, () => logit(`Listening Socket on ${8070}`));

const io: SocketIO.Server = socketIO(server);

interface ServerState {
  users: ServerStateUser[];
}

let serverState: ServerState = {
  users: []
};

io.on("connection", function(socket: SocketIO.Socket) {
  // try to login
  socket.on("login", (data: any) => {
    logit("connection attempt");

    // create new user with random id
    const currentSocketUser = {
      username: data.username,
      userid: generate("0123456789abcdefghijklmnopqrstuvwxyz", 20)
    };

    // is user already loggined
    if (isUserAlreadyLoggined(data.username)) {
      logit("attempt rejected - user exist");
      socket.emit("error message", { message: "user already logged" });
      socket.disconnect();
    } else {
      serverState.users.push(currentSocketUser);
      socket.emit("successfully logged in", currentSocketUser);
    }

    // cleanup in server state on disconnect
    socket.on("disconnect", () => {
      logit("disconnected");
      serverState.users = serverState.users.filter(
        (user: ServerStateUser) => user.userid !== currentSocketUser.userid
      );
    });
  });
});

// remove me -> send state to clients
setInterval(() => {
  io.emit("state", serverState);
}, 1000);

const isUserAlreadyLoggined = (username: string) =>
  serverState.users.some((user: ServerStateUser) => user.username === username);
