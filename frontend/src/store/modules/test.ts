import { AnyAction } from "redux";

const TEST = "TEST";
export const testAction = () => ({
  type: TEST
});

export const testReducer = (state = false, action: AnyAction) => {
  switch (action.type) {
    case TEST:
      return true;
    default:
      return state;
  }
};
