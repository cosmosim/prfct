import { login } from "../../api/socket";

export const loginRequestAction = (username: string) => login(username);
