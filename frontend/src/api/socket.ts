import * as io from "socket.io-client";

interface ISocketState {
  socket: SocketIOClient.Socket | null;
}
const state: ISocketState = {
  socket: io.connect()
};

export const login = (username: string) =>
  connectWithNessage("login", {
    username
  });

export const reconnect = (username: string, userid: string) =>
  connectWithNessage("reconnect", {
    username,
    userid
  });

export const connectWithNessage = (event: string, data: any) => {
  state.socket = io.connect(
    "/",
    {
      path: "/socket.io",
      reconnection: false
    }
  );

  state.socket.on("connect", () => {
    state.socket!.emit(event, data);
    console.log("connected");
  });

  state.socket.on("disconnect", () => {
    console.log("socket disconnected");
  });

  state.socket.on("state", (data: any) => {
    console.log("state", data);
  });

  state.socket.on("error message", (data: any) => {
    console.log("error message", data);
  });

  state.socket.on("successfully logged in", (data: any) => {
    console.log("successfully logged in", data);
  });
};
