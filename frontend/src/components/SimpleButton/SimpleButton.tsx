import React from "react";
import styles from "./SimpleButton.module.scss";

interface SimpleButtonProps {
  text: string;
  onClick?: () => void;
}
export const SimpleButton = ({ text, onClick }: SimpleButtonProps) => (
  <button className={styles.button} onClick={onClick} type="submit">
    {text}
  </button>
);
