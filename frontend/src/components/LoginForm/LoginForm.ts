import { connect } from "react-redux";
import { LoginFormComponent } from "./LoginFormComponent";
import { Dispatch } from "react";
import { testAction } from "../../store/modules/test";
import { AnyAction } from "redux";

export const LoginForm = connect(
  null,
  null
)(LoginFormComponent);
