import { SimpleInputField } from "../SimpleInputField/SimpleInputField";
import { SimpleButton } from "../SimpleButton/SimpleButton";
import React, { FormEvent } from "react";
import { loginRequestAction } from "../../store/modules/login";

interface LoginFormState {
  inputValue: string;
}

interface LoginFormProps {}
export class LoginFormComponent extends React.Component<
  LoginFormProps,
  LoginFormState
> {
  state: LoginFormState = {
    inputValue: ""
  };

  onSubmitHandler = (event: FormEvent<HTMLFormElement>) => {
    loginRequestAction(this.state.inputValue);
    event.preventDefault();
  };

  onInputChange = (inputValue: string) =>
    this.setState({
      ...this.state,
      inputValue: inputValue
    });

  render() {
    return (
      <form onSubmit={this.onSubmitHandler}>
        prfct simulation system
        <SimpleInputField onChange={this.onInputChange} />
        <SimpleButton text="login" />
      </form>
    );
  }
}
