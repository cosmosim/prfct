import React, { ChangeEvent } from "react";
import styles from "./SimpleInputField.module.scss";

interface SimpleInputFieldProps {
  onChange: (value: string) => void;
}
export const SimpleInputField = ({ onChange }: SimpleInputFieldProps) => {
  const onChangeHandler = (event: ChangeEvent<HTMLInputElement>) =>
    onChange(event.target.value);

  return (
    <input
      type="text"
      className={styles.input}
      onChange={onChangeHandler}
      spellCheck={false}
    />
  );
};
